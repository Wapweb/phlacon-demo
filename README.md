# Phalcon demo app

## Requirements

- docker and docker compose

## Quick start

- Copy config.example.php to config.php
```cp ./app/config/config.example.php ./app/config/config.php```
- Copy user.example.json to user.json
```cp ./app/data/user.example.json ./app/data/user.json```
- Init and start project:
 ```docker-compose up -d```
- Visit localhost:8000
- To login, visit localhost:8000/login and type test email and password:

email: admin@gmail.com  
password: 1111
