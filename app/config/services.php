<?php
declare(strict_types=1);

use Phalcon\Crypt;
use Phalcon\Escaper;
use Phalcon\Events\Manager;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Flash\Session;
use Phalcon\Http\RequestInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;
use Phalcon\Url as UrlResolver;
use PhalconDemo\Library\Auth\Auth;
use PhalconDemo\Library\Auth\FileEmailPasswordLoginAuth;
use PhalconDemo\Library\Auth\FileTokenLoginAuth;
use PhalconDemo\Library\PasswordHash\BcryptPasswordHashVerify;
use PhalconDemo\Library\PasswordHash\PasswordHashVerify;
use PhalconDemo\Library\Token\RandomBytesTokenGenerator;
use PhalconDemo\Library\Token\TokenGenerator;
use PhalconDemo\Library\User\Repository\FileUserFetchRepository;
use PhalconDemo\Library\User\Repository\UserFetchRepository;
use PhalconDemo\Library\User\Repository\UserPersistRepository;
use PhalconDemo\Library\User\Repository\FileUserPersistRepository;
use PhalconDemo\Library\User\UserLogin;
use PhalconDemo\Library\User\UserLogout;
use PhalconDemo\Library\UserToken\Repository\FileUserTokenRepository;
use PhalconDemo\Library\UserToken\Repository\UserTokenRepository;
use PhalconDemo\Library\UserToken\UserTokenFactory;
use PhalconDemo\Plugins\AuthPlugin;

$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

$di->setShared('dispatcher', function () {
    $eventsManager = new Manager();

    $eventsManager->attach('dispatch:beforeExecuteRoute', $this->get(AuthPlugin::class));

    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

$di->setShared(AuthPlugin::class, function () {
    return new AuthPlugin(
        $this->get(FileTokenLoginAuth::class),
        $this->get('cookies')
    );
});

$di->set('router', function () use ($di) {
    return include APP_PATH . "/config/router.php";
});

$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});


$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'path' => $config->application->cacheDir,
                'separator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});


$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'dbname' => $config->database->dbname,
    ];

    return new $class($params);
});


$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});


$di->set('flash', function () {
    $escaper = new Escaper();
    $flash = new Flash($escaper);
    $flash->setImplicitFlush(false);
    $flash->setCssClasses([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);

    return $flash;
});

$di->set('flashSession', function () {
    $escaper = new Escaper();
    $flash = new Session($escaper);
    $flash->setCssClasses([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);

    return $flash;
});


$di->setShared('session', function () {
    $session = new SessionManager();
    $files = new SessionAdapter([
        'savePath' => sys_get_temp_dir(),
    ]);
    $session->setAdapter($files);
    $session->start();

    return $session;
});

$di->setShared(UserFetchRepository::class, function () {
    $config = $this->getConfig();

    $filename = $config->database->user;

    return new FileUserFetchRepository($filename);
});

$di->setShared(UserTokenFactory::class, function () {
    return new UserTokenFactory();
});

$di->setShared(UserPersistRepository::class, function () {
    $config = $this->getConfig();

    $filename = $config->database->user;

    return new FileUserPersistRepository($filename);
});

$di->setShared(UserTokenRepository::class, function () {
    $config = $this->getConfig();

    $filename = $config->database->userToken;

    $tokenFactory = $this->get(UserTokenFactory::class);

    return new FileUserTokenRepository($filename, $tokenFactory);
});

$di->set(PasswordHashVerify::class, BcryptPasswordHashVerify::class);
$di->set(TokenGenerator::class, RandomBytesTokenGenerator::class);

$di->setShared(FileEmailPasswordLoginAuth::class, function () {
    return new FileEmailPasswordLoginAuth(
        $this->get(UserFetchRepository::class),
        $this->get(PasswordHashVerify::class)
    );
});

$di->setShared(FileTokenLoginAuth::class, function () {
    return new FileTokenLoginAuth(
        $this->get(UserTokenRepository::class),
        $this->get(UserFetchRepository::class)
    );
});

$di->setShared(UserLogin::class, function () {
    return new UserLogin(
        $this->get(FileEmailPasswordLoginAuth::class),
        $this->get(TokenGenerator::class),
        $this->get(UserTokenRepository::class),
        $this->get(UserTokenFactory::class)
    );
});

$di->setShared(UserLogout::class, function () {
    return new UserLogout(
        $this->get(UserTokenRepository::class),
        $this->get('cookies')
    );
});

$di->set('crypt', function () {
    $config = $this->getConfig();

    $crypt = new Crypt();
    $crypt->setKey($config->cryptKey);
    return $crypt;
});

$di->setShared(RememberMe::class, function () {
    return new RememberMe($this->get('cookies'));
});

