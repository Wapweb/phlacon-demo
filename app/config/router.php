<?php
/** @var $di */

use Phalcon\Mvc\Router;

$router = new Router(false);

$router->removeExtraSlashes(true);

$router->addGet('/', [
    'controller' => 'index'
]);

$router->addGet('/login', [
    'controller' => 'login'
]);

$router->addGet('/logout', [
    'controller' => 'logout'
]);


$router->addPost('/login/submit', [
    'controller' => 'login',
    'action' => 'submit'
]);

$router->notFound([
    'controller' => 'not-found'
]);

return $router;

