<?php
/** @var $config */

use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir
    ]
);

$loader->registerNamespaces(
    [
        'PhalconDemo\Library' => APP_PATH . '/library',
        'PhalconDemo\Models' => APP_PATH . '/models',
        'PhalconDemo\Forms' => APP_PATH . '/forms',
        'PhalconDemo\Plugins' => APP_PATH . '/plugins',
    ]
);

$loader->register();
