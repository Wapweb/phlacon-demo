<?php

declare(strict_types=1);

namespace PhalconDemo\Library\User;


use PhalconDemo\Library\Register\Register;
use PhalconDemo\Library\Register\RegisterRequestFactory;
use PhalconDemo\Library\Register\RequestValidationException;
use PhalconDemo\Library\Register\PersistenceException;
use PhalconDemo\Library\Token\TokenGeneratorException;
use PhalconDemo\Library\UserActivation\UserActivationSend;
use PhalconDemo\Library\UserActivation\UserActivationSendFailedException;
use PhalconDemo\Library\UserMappers\UserToUserCreatedJsonResponseMapper;
use Illuminate\Http\Request;

class CreateNewUserUseCase
{
    private Register $registerService;

    private RegisterRequestFactory $registerRequestFactory;

    private UserToUserCreatedJsonResponseMapper $userToJsonMapper;

    private UserActivationSend $userActivationSend;

    /**
     * Create a new controller instance.
     *
     * @param Register $registerService
     * @param RegisterRequestFactory $registerRequestFactory
     * @param UserToUserCreatedJsonResponseMapper $userToJsonMapper
     * @param UserActivationSend $activationEmailService
     */
    public function __construct(
        Register $registerService,
        RegisterRequestFactory $registerRequestFactory,
        UserToUserCreatedJsonResponseMapper $userToJsonMapper,
        UserActivationSend $activationEmailService
    )
    {
        $this->registerService = $registerService;
        $this->registerRequestFactory = $registerRequestFactory;
        $this->userToJsonMapper = $userToJsonMapper;
        $this->userActivationSend = $activationEmailService;
    }

    /**
     * @param Request $request
     * @return array
     * @throws UserActivationSendFailedException
     * @throws PersistenceException
     * @throws RequestValidationException
     * @throws TokenGeneratorException
     */
    public function execute(Request $request): array
    {
        /** @var array $params */
        $params = $request->all();

        $registerRequest = $this->registerRequestFactory->fromArray($params);

        $user = $this->registerService->register($registerRequest);

        $this->userActivationSend->send($user);

        return $this->userToJsonMapper->map($user);
    }
}
