<?php

namespace PhalconDemo\Library\User;


use PhalconDemo\Library\Auth\AuthException;
use PhalconDemo\Library\Auth\AuthInvalidCredException;
use PhalconDemo\Library\Auth\LoginAuth;
use PhalconDemo\Library\Token\TokenGenerator;
use PhalconDemo\Library\Token\TokenGeneratorException;
use PhalconDemo\Models\User;
use PhalconDemo\Library\UserToken\Repository\UserTokenPersistException;
use PhalconDemo\Library\UserToken\Repository\UserTokenRepository;
use PhalconDemo\Library\UserToken\UserToken;
use PhalconDemo\Library\UserToken\UserTokenFactory;

class UserLogin
{
    private LoginAuth $auth;

    private TokenGenerator $tokenGenerator;

    private UserTokenRepository $userTokenRepository;

    private UserTokenFactory $userTokenFactory;

    public function __construct(
        LoginAuth $auth,
        TokenGenerator $tokenGenerator,
        UserTokenRepository $userTokenRepository,
        UserTokenFactory $userTokenFactory
    ) {
        $this->auth = $auth;
        $this->tokenGenerator = $tokenGenerator;
        $this->userTokenFactory = $userTokenFactory;
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * @param string $password
     * @param string $email
     * @return UserToken
     * @throws AuthException
     * @throws AuthInvalidCredException
     * @throws TokenGeneratorException
     * @throws UserTokenPersistException
     */
    public function auth(string $password, string $email): UserToken
    {
        $cred = [
            'password' => $password,
            'email' => $email
        ];

        $valid = $this->auth->authenticate($cred);

        if (!$valid) {
            throw new AuthInvalidCredException('Invalid credentials');
        }

        $token = $this->tokenGenerator->generate();

        /** @var User $user */
        $user = $this->auth->getIdentity();

        $tokenModel = $this->userTokenFactory->create($token, $user->id());

        $this->userTokenRepository->persist($tokenModel);

        return $tokenModel;
    }
}
