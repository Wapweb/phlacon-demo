<?php


namespace PhalconDemo\Library\User;


use Phalcon\Http\Response\Cookies;
use PhalconDemo\Library\UserToken\Repository\UserTokenRepository;
use PhalconDemo\Library\UserToken\UserToken;
use PhalconDemo\Models\LoggableInterface;
use PhalconDemo\Models\User;
use PhalconDemo\Plugins\AuthPlugin;

class UserLogout
{
    private UserTokenRepository $userTokenRepository;

    private Cookies $cookies;


    public function __construct(UserTokenRepository $userTokenRepository, Cookies $cookies)
    {
        $this->userTokenRepository = $userTokenRepository;
        $this->cookies = $cookies;
    }

    /**
     * @param LoggableInterface $user
     * @return bool
     * @throws UserLogoutException
     */
    public function logout(LoggableInterface $user): bool
    {
        if (!$user->isLogged()) {
            throw new UserLogoutException('User is not logged');
        }

        $this->userTokenRepository->clearByUserId($user->id());

        $this->cookies->set(AuthPlugin::COOKIE_NAME, null, time() - 3600);
        $this->cookies->send();

        return true;
    }
}
