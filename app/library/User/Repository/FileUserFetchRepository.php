<?php

namespace PhalconDemo\Library\User\Repository;


use PhalconDemo\Library\Repository\File\JsonFileOperationsTrait;
use PhalconDemo\Models\User;

class FileUserFetchRepository implements UserFetchRepository
{
    use JsonFileOperationsTrait;

    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param string $id
     * @return User
     * @throws UserNotFoundException
     */
    public function fetchById(string $id): User
    {
        $data = $this->searchBy($this->filename, 'id', $id);

        if (empty($data)) {
            throw new UserNotFoundException(sprintf('User with id `%s` not found', $id));
        }

        return $this->mapToModel($data);
    }

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    public function fetchByEmail(string $email): User
    {
        $data = $this->searchBy($this->filename, 'email', $email);

        if (empty($data)) {
            throw new UserNotFoundException(sprintf('User with email `%s` not found', $email));
        }

        return $this->mapToModel($data);
    }


    private function mapToModel(array $data): User
    {
        return new User($data['id'], $data['email'], $data['password']);
    }
}
