<?php

namespace PhalconDemo\Library\User\Repository;

use PhalconDemo\Library\Repository\File\JsonFileOperationsTrait;
use PhalconDemo\Models\User;

class FileUserPersistRepository implements UserPersistRepository
{
    use JsonFileOperationsTrait;

    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param User $user
     * @return bool
     * @throws UserPersistException
     */
    public function persist(User $user): bool
    {
        $data = $this->mapUserToArray($user);

        $result = $this->save($this->filename, $data);

        if ($result === false) {
            throw new UserPersistException('Failed to persist user');
        }

        return true;
    }

    private function mapUserToArray(User $user): array
    {
        return [
            'email' => $user->email(),
            'password' => $user->password(),
            'id' => $user->id(),
        ];
    }
}
