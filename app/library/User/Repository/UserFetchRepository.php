<?php


namespace PhalconDemo\Library\User\Repository;


use PhalconDemo\Models\User;

interface UserFetchRepository
{
    /**
     * @param string $id
     * @return User
     * @throws UserNotFoundException
     */
    function fetchById(string $id): User;

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    function fetchByEmail(string $email): User;
}
