<?php

namespace PhalconDemo\Library\User\Repository;

use PhalconDemo\Models\User;

interface UserPersistRepository
{
    function persist(User $user);
}
