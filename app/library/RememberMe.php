<?php


use Phalcon\Http\Response\Cookies;
use PhalconDemo\Library\UserToken\UserToken;

class RememberMe
{
    private Cookies $cookies;

    public function __construct(Cookies $cookies)
    {
        $this->cookies = $cookies;
    }

    public function remember(UserToken $userToken): bool
    {
        $this->cookies->set('user_token', $userToken->token(), time() + 3600 * 24);
        $this->cookies->send();

        return true;
    }
}
