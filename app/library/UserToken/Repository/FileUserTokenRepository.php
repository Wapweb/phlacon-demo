<?php


namespace PhalconDemo\Library\UserToken\Repository;

use PhalconDemo\Library\Repository\File\JsonFileOperationsTrait;
use PhalconDemo\Library\UserToken\UserToken;
use PhalconDemo\Library\UserToken\UserTokenFactory;

class FileUserTokenRepository implements UserTokenRepository
{
    use JsonFileOperationsTrait;

    private string $filename;

    private UserTokenFactory $userTokenFactory;

    public function __construct(string $filename, UserTokenFactory $userTokenFactory)
    {
        $this->filename = $filename;
        $this->userTokenFactory = $userTokenFactory;
    }

    /**
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    public function fetchByToken(string $token): UserToken
    {
        $data = $this->searchBy($this->filename, 'token', $token);

        if (empty($data)) {
            throw new UserTokenNotFound(sprintf('User token by token `%s` not found', $token));
        }

        return $this->mapToModel($data);
    }

    public function persist(UserToken $userToken): bool
    {
        $data = $this->mapToData($userToken);

        $result = $this->save($this->filename, $data);

        if (!$result) {
            throw new UserTokenPersistException('Failed to persist user token');
        }

        return true;
    }

    public function clearByUserId(string $userId): bool
    {
        $res = $this->deleteBy($this->filename, 'user_id', $userId);

        return $res === false ? false : true;
    }

    private function mapToModel(array $data): UserToken
    {
        /** @var \DateTime $expiredAt */
        $expiredAt = !empty($data['expired_at']) ? $data['expired_at']->toDateTime() : null;

        return $this->userTokenFactory->create($data['token'], $data['user_id'], $expiredAt);
    }

    private function mapToData(UserToken $token)
    {
        $expiredAt = $token->expiredAt();

        return [
            'token' => $token->token(),
            'user_id' => $token->userId(),
            'expired_at' => empty($expiredAt) ? null : $expiredAt->getTimestamp()
        ];
    }
}
