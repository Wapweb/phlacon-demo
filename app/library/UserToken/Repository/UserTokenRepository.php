<?php


namespace PhalconDemo\Library\UserToken\Repository;


use PhalconDemo\Library\UserToken\UserToken;

interface UserTokenRepository
{
    /**
     * @param string $token
     * @return UserToken
     * @throws UserTokenNotFound
     */
    function fetchByToken(string $token): UserToken;

    /**
     * @param UserToken $token
     * @return bool
     * @throws UserTokenPersistException
     */
    function persist(UserToken $token): bool;

    function clearByUserId(string $userId): bool;
}
