<?php


namespace PhalconDemo\Library\UserToken;


class UserTokenFactory
{
    public function create(string $token, string $userId, \DateTime $expiredAt = null): UserToken
    {
        return new UserToken($token, $userId, $expiredAt);
    }
}
