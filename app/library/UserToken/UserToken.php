<?php


namespace PhalconDemo\Library\UserToken;


class UserToken
{
    private string $token;
    private string $userId;
    private ?\DateTime $expiredAt;

    public function __construct(string $token, string $userId, \DateTime $expiredAt = null)
    {
        $this->token = $token;
        $this->userId = $userId;
        $this->expiredAt = $expiredAt;
    }

    public function token(): string
    {
        return $this->token;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function isExpired(\DateTime $currentTime = null): bool
    {
        if (!$this->expiredAt) {
            return false;
        }

        $currentTime = $currentTime ?? new \DateTime();

        return $currentTime > $this->expiredAt;
    }

    public function expiredAt(): ?\DateTime
    {
        return $this->expiredAt;
    }
}
