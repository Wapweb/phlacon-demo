<?php


namespace PhalconDemo\Library\Auth;


use PhalconDemo\Library\PasswordHash\PasswordHashVerify;
use PhalconDemo\Library\User\Repository\UserFetchRepository;

class FileEmailPasswordLoginAuth implements LoginAuth
{
    private UserFetchRepository $userFetchRepository;

    private PasswordHashVerify $passwordHashVerify;

    private $identity = null;

    public function __construct(
        UserFetchRepository $userFetchRepository,
        PasswordHashVerify $passwordHashVerify
    )
    {
        $this->userFetchRepository = $userFetchRepository;
        $this->passwordHashVerify = $passwordHashVerify;
    }

    public function authenticate(array $cred = null): bool
    {
        if (empty($cred) || !isset($cred['password']) || !isset($cred['email'])) {
            throw new AuthException('Invalid $cred: password and email fields are required');
        }

        $pass = $cred['password'];

        $email = $cred['email'];

        try {
            $user = $this->userFetchRepository->fetchByEmail($email);
        } catch (\Exception $e) {
            return false;
        }

        $valid = $this->passwordHashVerify->verify($pass, $user->password());

        if ($valid) {
            $this->identity = $user;
        }

        return $valid;
    }

    public function getIdentity()
    {
        return $this->identity;
    }

}
