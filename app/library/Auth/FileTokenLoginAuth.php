<?php


namespace PhalconDemo\Library\Auth;


use PhalconDemo\Library\User\Repository\UserFetchRepository;
use PhalconDemo\Library\UserToken\Repository\UserTokenRepository;
use PhalconDemo\Models\User;

class FileTokenLoginAuth implements LoginAuth
{
    private UserTokenRepository $userTokenRepository;

    private UserFetchRepository $userFetchRepository;

    private User $identity;

    public function __construct(UserTokenRepository $userTokenRepository, UserFetchRepository $userFetchRepository)
    {
        $this->userTokenRepository = $userTokenRepository;
        $this->userFetchRepository = $userFetchRepository;
    }

    public function authenticate(array $cred): bool
    {
        try {
            $token = $cred['token'] ?? '';

            $tokenModel = $this->userTokenRepository->fetchByToken($token);

            $user = $this->userFetchRepository->fetchById($tokenModel->userId());

            $this->identity = $user;

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getIdentity()
    {
        return $this->identity;
    }
}
