<?php


namespace PhalconDemo\Library\Auth;


interface Auth
{
    /**
     * @param array $cred
     * @return bool
     * @throws AuthException
     */
    function authenticate(array $cred): bool;

    /**
     * @return mixed
     */
    function getIdentity();
}
