<?php


namespace PhalconDemo\Library\Token;


interface TokenGenerator
{
    /**
     * @return string
     * @throws TokenGeneratorException
     */
    function generate(): string;
}
