<?php


namespace PhalconDemo\Library\Token;


class RandomBytesTokenGenerator implements TokenGenerator
{
    public const LENGTH = 32;

    function generate(): string
    {
        try {
            return bin2hex(random_bytes(self::LENGTH));
        } catch (\Exception $exception) {
            throw new TokenGeneratorException($exception->getMessage());
        }
    }
}
