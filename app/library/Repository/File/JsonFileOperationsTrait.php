<?php

namespace PhalconDemo\Library\Repository\File;

trait JsonFileOperationsTrait
{
    private function searchBy(string $filename, string $fieldName, $fieldValue): ?array
    {
        $rows = $this->getAll($filename);

        foreach ($rows as $row) {
            if ($row[$fieldName] === $fieldValue) {
                return $row;
            }
        }

        return null;
    }

    /**
     * @param string $filename
     * @param array $data
     * @return false|int
     */
    private function save(string $filename, array $data)
    {
        $rows = $this->getAll($filename);
        $rows[] = $data;

        return file_put_contents($this->filename, json_encode($rows));
    }

    /**
     * @param string $filename
     * @param string $fieldName
     * @param $fieldValue
     * @return bool|int false if failed to save otherwise returns number of deleted items
     */
    private function deleteBy(string $filename, string $fieldName, $fieldValue)
    {
        $rows = $this->getAll($filename);

        $newRows = [];
        $deleted = 0;
        foreach ($rows as $row) {
            if ($row[$fieldName] === $fieldValue) {
                $deleted++;
                continue;
            }

            $newRows[] = $row;
        }

        $result = file_put_contents($this->filename, json_encode($newRows));

        return $result === false ? $result : $deleted;
    }

    private function getAll(string $filename): array
    {
        if (!file_exists($filename)) {
            return [];
        }

        $content = file_get_contents($filename);

        return json_decode($content, true);
    }
}
