<?php


namespace PhalconDemo\Library\PasswordHash;


interface PasswordHashVerify
{
    function verify(string $password, string $hash): bool;
}
