<?php


namespace PhalconDemo\Library\PasswordHash;


interface PasswordHashGenerator
{
    function generate(string $password): string;
}
