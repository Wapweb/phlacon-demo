<?php


namespace PhalconDemo\Library\PasswordHash;


class BcryptPasswordHashGenerator implements PasswordHashGenerator
{
    function generate(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}
