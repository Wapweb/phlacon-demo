<?php


namespace PhalconDemo\Library\PasswordHash;


class BcryptPasswordHashVerify implements PasswordHashVerify
{

    function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
