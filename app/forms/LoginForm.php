<?php


namespace PhalconDemo\Forms;


use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends Form
{
    public function initialize()
    {
        $password = new Password('password');
        $password->addValidator(new PresenceOf());

        $email = new Email('email');
        $email->addValidator(new PresenceOf());

        $this
            ->add($password)
            ->add($email);
    }

    public function getMessagesAsString($glue = ', ')
    {
        $messages = parent::getMessages(true);

        $result = [];

        foreach ($messages as $message) {
            $result[] = $message;
        }

        return implode($glue, $result);
    }
}
