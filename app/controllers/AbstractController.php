<?php


use Phalcon\Mvc\Controller;
use PhalconDemo\Models\LoggableInterface;

class AbstractController extends Controller
{
    protected function getCurrentUser(): LoggableInterface
    {
        return $this->di->get('currentUser');
    }
}