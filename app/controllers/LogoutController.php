<?php


use PhalconDemo\Library\User\UserLogout;

class LogoutController extends AbstractController
{
    private ?UserLogout $userLogoutService;

    public function initialize()
    {
        $this->userLogoutService = $this->di->get(UserLogout::class);
    }

    public function indexAction()
    {
        $this->userLogoutService->logout($this->getCurrentUser());

        $this->response->redirect('/');
    }
}