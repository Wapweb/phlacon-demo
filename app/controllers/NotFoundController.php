<?php


use Phalcon\Mvc\Controller;

class NotFoundController extends Controller
{
    public function indexAction()
    {
        return $this->response
            ->setStatusCode(404)
            ->setContent('<h1>Not found</h1>');
    }
}
