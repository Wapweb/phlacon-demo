<?php

use PhalconDemo\Forms\LoginForm;
use PhalconDemo\Library\User\UserLogin;

class LoginController extends AbstractController
{
    private ?LoginForm $loginForm;

    private ?UserLogin $loginService;

    private ?RememberMe $rememberMeService;

    public function initialize()
    {
        $this->loginForm = $this->di->get(LoginForm::class);
        $this->loginService = $this->di->get(UserLogin::class);
        $this->rememberMeService = $this->di->get(RememberMe::class);
    }

    public function beforeExecuteRoute($dispatcher)
    {
        if ($this->getCurrentUser()->isLogged()) {
            $this->response->redirect('/');

            return true;
        }
    }

    public function indexAction()
    {
    }

    public function submitAction()
    {
        $data = $this->request->getPost();

        if (!$this->loginForm->isValid($data)) {
            $err = $this->loginForm->getMessagesAsString();

            $this->flashSession->error($err);

            return $this->response->redirect('/login');
        }

        $pass = $this->loginForm->getValue('password');
        $email = $this->loginForm->getValue('email');

        try {
            $userToken = $this->loginService->auth($pass, $email);

            $this->rememberMeService->remember($userToken);

            return $this->response->redirect('/');
        } catch (\Exception $e) {
            $this->flashSession->error($e->getMessage());

            return $this->response->redirect('/login');
        }
    }
}