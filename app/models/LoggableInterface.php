<?php


namespace PhalconDemo\Models;


interface LoggableInterface
{
    function isLogged(): bool;

    function id(): string;
}