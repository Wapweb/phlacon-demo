<?php


namespace PhalconDemo\Models;


class Guest implements LoggableInterface
{
    public function isLogged(): bool
    {
        return false;
    }

    public function id(): string
    {
        return '';
    }
}