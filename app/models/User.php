<?php

namespace PhalconDemo\Models;

class User implements LoggableInterface
{
    private string $id;

    private string $email;

    private string $password;

    private bool $authenticated = false;

    public function __construct(string $id, string $email, string $password)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function authenticate()
    {
        $this->authenticated = true;
    }

    public function isLogged(): bool
    {
        return  $this->authenticated;
    }
}
