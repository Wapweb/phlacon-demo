<?php

namespace PhalconDemo\Plugins;

use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Http\Response\Cookies;
use Phalcon\Mvc\Dispatcher;
use PhalconDemo\Library\Auth\FileTokenLoginAuth;
use PhalconDemo\Models\Guest;
use PhalconDemo\Models\LoggableInterface;

class AuthPlugin extends Injectable
{
    private FileTokenLoginAuth $tokenAuth;

    private Cookies $cookies;

    public const COOKIE_NAME = 'user_token';

    public function __construct(FileTokenLoginAuth $tokenAuth, Cookies $cookies)
    {
        $this->tokenAuth = $tokenAuth;
        $this->cookies = $cookies;
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $token = $this->cookies->get(self::COOKIE_NAME)->getValue();

        if (!$token) {
            $this->injectUser(new Guest());

            return true;
        }

        $res = $this->tokenAuth->authenticate(['token' => $token]);

        if (!$res) {
            $this->injectUser(new Guest());

            $this->cookies->set(self::COOKIE_NAME, null, time() - 3600);

            $this->cookies->send();

            return false;
        }

        $user = $this->tokenAuth->getIdentity();
        $user->authenticate();

        $this->injectUser($user);

        return true;
    }

    private function injectUser(LoggableInterface $loggable)
    {
        $this->getDI()->set('currentUser', $loggable);
        $this->getDI()->get('view')->setVar('currentUser', $loggable);
    }
}
