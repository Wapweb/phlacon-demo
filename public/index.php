<?php

declare(strict_types=1);

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $di = new FactoryDefault();

    include APP_PATH . '/config/services.php';

    $config = $di->get('config');

    include APP_PATH . '/config/loader.php';

    $application = new Application($di);

    //    // Handle the request
    $request = new Phalcon\Http\Request();

    $response = $application->handle($request->getURI());

    $response->send();
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}


//
//use Phalcon\Loader;
//use Phalcon\Mvc\View;
//use Phalcon\Mvc\Application;
//use Phalcon\Di\FactoryDefault;
//use Phalcon\Mvc\Url as UrlProvider;
//
//// Define some absolute path constants to aid in locating resources
//define('BASE_PATH', dirname(__DIR__));
//define('APP_PATH', BASE_PATH . '/app');
//
//// Register an autoloader
//$loader = new Loader();
//
//$loader->registerDirs(
//    [
//        APP_PATH . '/controllers/',
//        APP_PATH . '/models/',
//    ]
//);
//
//$loader->register();
//
//
//// Create a DI
//$di = new FactoryDefault();
//
//// Setup the view component
//$di->set(
//    'view',
//    function () {
//        $view = new View();
//        $view->setViewsDir(APP_PATH . '/views/');
//        return $view;
//    }
//);
//
//// Setup a base URI
//$di->set(
//    'url',
//    function () {
//        $url = new UrlProvider();
//        $url->setBaseUri('/');
//        return $url;
//    }
//);
//
//$application = new Application($di);
//
//try {
//    // Handle the request
//    $request = new Phalcon\Http\Request();
//
//    $response = $application->handle($request->getURI());
//
//    $response->send();
//} catch (\Exception $e) {
//    echo 'Exception: ', $e->getMessage();
//}